#include <iostream>
#include <vector>
#include <map>
#include <algorithm>
#include <cmath>
#include <cstdlib>
using namespace std;

class client {
    //client class containing information about the client and asking about the dog being up to date on all the shots
private: 
    string clientName;
    string phoneNum;
    string address;
    bool isDogUpToDateOnShots;
    
public:
    //constructor for the client class - it takes the name, phoneNum, address, and shots as parameters and initializes the private members of the class.
    client(string clientName, string phoneNum, string address, bool isDogUpToDateOnShots) :
        clientName(clientName), phoneNum(phoneNum), address(address), isDogUpToDateOnShots(isDogUpToDateOnShots) {}
    
    //getters and setters for client info
    void setClientName(string clientName) { this->clientName = clientName; }
    void setPhoneNum(string phoneNum) { this->phoneNum = phoneNum; }
    void setAddress(string address) { this-> address = address; }
    void setDogHasShots(bool isDogUpToDateOnShots) { this->isDogUpToDateOnShots = isDogUpToDateOnShots; }
    
    string displayYesNo(bool v) {
        //convert 0s and 1s to yes or no for displaying full info
        if (v == true) {
            return "Yes";
        }
        else {
            return "No";
        }
    }
    
    void displayClientInfo() {
        //displaying all client information in profile form
        cout << "Client Name: " << clientName << endl;
        cout << "Phone Number: " << phoneNum << endl;
        cout << "Address: " << address << endl;
        cout << "Dog Up to Date on Shots: " << displayYesNo(isDogUpToDateOnShots) << endl;
    }
};

class dog {
    //dog class containing information about dog and its behaviors
private:
    string name;
    string breed;
    int age;
    int weight;
    bool isHyper;
    bool doesBite;
    bool hasAnxiety;
    
public:
    //constructor for the do class - it takes the name, breed, age, weight, hyper, anxiety, and bite as parameters and initializes the private members of the class.
    dog(string name, string breed, int age, int weight, bool isHyper, bool doesBite, bool hasAnxiety) :
        name(name), breed(breed), age(age), weight(weight), isHyper(isHyper), doesBite(doesBite), hasAnxiety(hasAnxiety) {}
    
    //gettes for the dog's information
    string getDogName() { return name; }
    string getDogBreed() { return breed; }
    int getDogAge() { return age; }
    int getDogWeight() { return weight; }
    bool isDogHyper() { return isHyper; }
    bool doesDogBite() { return doesBite; }
    bool doesDogHaveAnxiety() { return hasAnxiety; }
    
    string displayYesNo(bool v) {
        //convert 0s and 1s to yes or no for displaying full info
        if (v == true) {
            return "Yes";
        }
        else {
            return "No";
        }
    }
    
    void displayDogInfo() {
        //displaying all dog's information in profile form
        cout << "Name: " << name << endl;
        cout << "Breed: " << breed << endl;
        cout << "Age: " << age << endl;
        cout << "Weight: " << weight << endl;
        cout << "Is Hyper: " << displayYesNo(isHyper) << endl;
        cout << "Bites: " << displayYesNo(doesBite) << endl;
        cout << "Has Anxiety: " << displayYesNo(hasAnxiety) << endl;
    }
};

class dogSitter {
    //dogSitter class containing dog sitter information and what behaviors they are able to handle
private:
    string firstName;
    string lastName;
    vector<string> dogBreeds;
    int minAge;
    int maxAge;
    int minWeightAccepted;
    int maxWeightAccepted;
    bool handlesHyper;
    bool handlesBites;
    bool handlesAnxiety;
    vector<int> monthsAvailable;
    vector<int> daysAvailable;
    
public:
    //constructor for the dogSitter class - the members of the private section as parameters and initializes the private members of the class.
    dogSitter(string firstName, string lastName, vector<string> dogBreeds, int minAge, int maxAge, int minWeightAccepted, int maxWeightAccepted, bool handlesHyper, bool handlesBites, bool handlesAnxiety, vector<int> monthsAvailable, vector<int> daysAvailable) :
        firstName(firstName), lastName(lastName), dogBreeds(dogBreeds), minAge(minAge), maxAge(maxAge), minWeightAccepted(minWeightAccepted), maxWeightAccepted(maxWeightAccepted), handlesHyper(handlesHyper), handlesBites(handlesBites), handlesAnxiety(handlesAnxiety), monthsAvailable(monthsAvailable), daysAvailable(daysAvailable)  {}
    
    //setters and getters for dog sitter information
    void setFirstName(string firstName) { this->firstName = firstName; }
    void setLastName(string lastName) { this->lastName = lastName; }
    void setMinAge(int minAge) { this->minAge = minAge; }
    void setMaxAge(int maxAge) { this->maxAge = maxAge; }
    void setMinWeight(int minWeightAccepted) { this->minWeightAccepted = minWeightAccepted; }
    void setMaxWeight(int maxWeightAccepted) { this->maxWeightAccepted = maxWeightAccepted; }
    void setHandlesHyper(bool handlesHyper) { this->handlesHyper = handlesHyper; }
    void setHandlesBites(bool handlesBites) { this->handlesBites = handlesBites; }
    void setHandlesAnxiety(bool handlesAnxiety) { this->handlesAnxiety = handlesAnxiety; }
    void setBreeds(vector<string> dogBreeds) { this->dogBreeds = dogBreeds; }
    void setMonths(vector<int> monthsAvailable) { this->monthsAvailable = monthsAvailable; }
    void setDays(vector<int> daysAvailable) { this->daysAvailable = daysAvailable; }
    
    string getFullName() { return firstName + " " + lastName; }
    int getMinAge() { return minAge; }
    int getMaxAge() { return maxAge; }
    int getMinWeight() { return minWeightAccepted; }
    int getMaxWeight() { return maxWeightAccepted; }
    bool getHandlesHyper() { return handlesHyper; }
    bool getHandlesBites() { return handlesBites; }
    bool getHandlesAnxiety() { return handlesAnxiety; }
    
    vector<string> getBreeds() {
        //getting breeds in vector
        return dogBreeds;
    }
    
    void printBreeds() {
        //printing breeds one by one (comma seperated) for displaying in sitter profile
        for (int i=0; i<dogBreeds.size(); i++) {
            cout << dogBreeds[i] << ", ";
        }
    }
    
    void displayDatesAvailable() {
        //displaying all dates sitter is available
        //we can change method if we prefer
        map<int, string> months = {
            {1, "January"}, {2, "February"}, {3, "March"}, {4, "April"}, {5, "May"}, {6, "June"},
            {7, "July"}, {8, "August"}, {9, "September"}, {10, "October"}, {11, "November"}, {12, "December"}
        };
        
        for (int i = 0; i < monthsAvailable.size(); i++) {
            if (months.find(monthsAvailable[i]) != months.end()) {
                string month = months[monthsAvailable[i]];
                int startDay = daysAvailable[i * 2];
                int endDay = daysAvailable[i * 2 + 1];
                cout << month << " " << startDay << " - " << endDay << endl;
            }
        }
    }
    
    string displayYesNo(bool v) {
        //use same function in sitter class for displaying info
        if (v == true) {
            return "Yes";
        }
        else {
            return "No";
        }
    }
    
    void displaySitterInfo() {
        //display all information in profile format
        cout << "Name: " << firstName << " " << lastName << endl;
        cout << "Preferred Breeds: ";
        printBreeds();
        cout << endl;
        cout << "Minimum and Maximum Age Preferred: " << minAge << " - " << maxAge << endl;
        cout << "Minimum and Maximum Weight Preferred: " << minWeightAccepted << " - " << maxWeightAccepted << endl;
        cout << "Handles Hyper Dogs: " << displayYesNo(handlesHyper) << endl;
        cout << "Handles Biters: " << displayYesNo(handlesBites) << endl;
        cout << "Handles Dogs with Anxiety: " << displayYesNo(handlesAnxiety) << endl;
        cout << "Dates Available: ";
        displayDatesAvailable();
        cout << endl;
    }
    
};

class job {
    //class job for booking jobs between client and sitter
private:
    dogSitter sitterInput; //the dog sitter assigned to the job
    client clientInput; //the client who booked the job
    string dropOffTime;
    string estimatedPickUpTime;
    double quote;
    
public:
    job(const dogSitter& sitterInput, const client& clientInput, string dropOffTime, string estimatedPickUpTime, double quote)
        : sitterInput(sitterInput), clientInput(clientInput), dropOffTime(dropOffTime), estimatedPickUpTime(estimatedPickUpTime), quote(quote) { }
    
    //setters for the drop-off time estimated pick up time, and quote
    void setDropOffTime(string dropOffTime) { this->dropOffTime = dropOffTime; }
    void setEstimatedPickUpTime(string estimatedPickUpTime) { this->estimatedPickUpTime = estimatedPickUpTime; }
    void setQuote(double quote) { this->quote = quote; }
    //getters for the drop-off time estimated pick up time, and quote
    string getDropOffTime() { return dropOffTime; }
    string getEstimatedPickUpTime() { return estimatedPickUpTime; }
    double getQuote() { return quote; }
    
    void displayJobInfo() {
        //display all the information in the profile format
        cout << "Sitter's Information: " << endl;
        sitterInput.displaySitterInfo();
        cout << "Client's Information: " << endl;
        clientInput.displayClientInfo();
        cout << "Drop off Time: " << dropOffTime << endl;
        cout << "Pick up Time: " << estimatedPickUpTime << endl;
        cout << "Quote: $" << quote << endl;
    }
    
};

string getDropOffTimeInput() {
    //this function prompts the user for a drop-off time, validates the input, and returns the drop-off time as a string
    string dropOffTime;
    
    cout << "Enter drop off time (ex. 2:00PM or 8:30AM): " << endl;
    cin >> dropOffTime;
    
    //declare variables to store he hour, minute, and AM/PM indicator
    int hour, minute;
    char amOrPm;
    
    //uses the sscanf() function to parse the drop-off time string into the hour, minute, and AM/PM indicator
    if (sscanf(dropOffTime.c_str(), "%d:%d%c", &hour, &minute, &amOrPm) == 3) {
        if ((hour >= 1 && hour <= 12) && (minute >= 0 && minute <= 59) &&
            (amOrPm == 'A' || amOrPm == 'P') && (dropOffTime.find("AM") != string::npos || dropOffTime.find("PM") != string::npos)) {
            return dropOffTime;
        }
    }
    
    cout << "Invalid input format. Please enter a valid time (ex. 2:00PM or 8:30AM)." << endl;
    return dropOffTime;
}

string getEstimatedPickUpTimeInput() {
     //this function prompts the user for an estimated pick-up time, validates the input, and returns the pick-up time as a string
    string estimatedPickUpTime;
    
    cout << "Enter pick up time (ex. 2:00PM or 8:30AM): " << endl;
    cin >> estimatedPickUpTime;
    
    //declare variables to store he hour, minute, and AM/PM indicator
    int hour, minute;
    char amOrPm;
    
    //uses the sscanf() function to parse the estimated pick-up time string into the hour, minute, and AM/PM indicator
    if (sscanf(estimatedPickUpTime.c_str(), "%d:%d%c", &hour, &minute, &amOrPm) == 3) {
        if ((hour >= 1 && hour <= 12) && (minute >= 0 && minute <= 59) &&
            (amOrPm == 'A' || amOrPm == 'P') && (estimatedPickUpTime.find("AM") != string::npos || estimatedPickUpTime.find("PM") != string::npos)) {
            return estimatedPickUpTime;
        }
    }
    
    cout << "Invalid input format. Please enter a valid time (ex. 2:00PM or 8:30AM)." << endl;
    return estimatedPickUpTime;
}

double getQuote(const string& dropOffTime, const string& estimatedPickUpTime) {
    //this function calculates the quote for a job based on the drop-off time, estimated pick-up time, and the hourly rate
    
    //declare variables to store the hour, minute, and AM/PM indicators for the drop-off tfime and estimated pick-up time
    int dropOffHour, dropOffMinute, pickUpHour, pickUpMinute;
    char dropOffAmOrPm, pickUpAmOrPm;
    
    //use the sscanf() function to parse the drop-off time and estimated pick-up time strings into their respective hours
    sscanf(dropOffTime.c_str(), "%d:%d%c", &dropOffHour, &dropOffMinute, &dropOffAmOrPm);
    sscanf(estimatedPickUpTime.c_str(), "%d:%d%c", &pickUpHour, &pickUpMinute, &pickUpAmOrPm);
    
    //convert times to 24-hour format
    if (dropOffAmOrPm == 'P') {
        dropOffHour += 12;
    }
    if (pickUpAmOrPm == 'P') {
        pickUpHour += 12;
    }
    
    //calculate the difference in hours
    double dropOffInMinutes = dropOffHour * 60 + dropOffMinute;
    double pickUpInMinutes = pickUpHour * 60 + pickUpMinute;
    double differenceInHours = (pickUpInMinutes - dropOffInMinutes) / 60.0;
    
    //calculate the quote based on the hourly rate of $15.
    double quote = differenceInHours * 15.0;
    return quote;
}

int matchingProcess(dog& d, dogSitter& s) {
    //function for matching process between dogs and dogsitters
    vector<string> breeds = s.getBreeds();
    double match = 0;
    
    //iterates through breeds -> if dog's breed is in accepted breeds: add match point
    for (int i=0; i<breeds.size(); i++) {
        if (breeds[i] == "ALL") {
            match += round((100/6) * pow(10, 2)) / pow(10, 2);
        }
        
        if (d.getDogBreed() == breeds[i]) {
            match += round((100/6) * pow(10, 2)) / pow(10, 2);
        }
    }
    
    //checks if dog size is within sitters constraints
    if (d.getDogWeight() <= s.getMaxWeight() && d.getDogWeight() >= s.getMinWeight()) {
        match += round((100/6) * pow(10, 2)) / pow(10, 2);
    }
    
    //do same with age
    if (d.getDogAge() <= s.getMaxAge() && d.getDogAge() >= s.getMinAge()) {
        match += round((100/6) * pow(10, 2)) / pow(10, 2);
    }
    
    //checks for hyper -> sitter will match if the values match or if they handle it and dog does not have it
    if ((d.isDogHyper() == s.getHandlesHyper()) || (d.isDogHyper() == false && s.getHandlesHyper() == true)) {
        match += round((100/6) * pow(10, 2)) / pow(10, 2);
    }
    
    //same for biting
    if ((d.doesDogBite() == s.getHandlesBites()) || (d.doesDogBite() == false && s.getHandlesHyper() == true)) {
        match += round((100/6) * pow(10, 2)) / pow(10, 2);
    }
    
    //same with anxiety
    if ((d.doesDogHaveAnxiety() == s.getHandlesAnxiety()) || (d.doesDogHaveAnxiety() == false && s.getHandlesAnxiety() == true)) {
        match += round((100/6) * pow(10, 2)) / pow(10, 2);
    }
    
    //return match %
    return match;
    
    //can use match % later to show dog owner a list of potential sitters in order of how well they match their needs
    //or could change it around to only show perfect matches    
};

string getClientNameInput() {
    //this function prompts the client for their name and return the name as a string
    
    //declaring a string variable to store the client's name
    string clientName;
    
    //Prompting the client for their name
    cout << "Enter Client's Name: " << endl;
    cin.ignore(); //ignore the first line of input, which is the newline character that was entered after the prompt
    getline(cin, clientName); //get the client's name from the user using the getline() function
    return clientName;
}

string getClientPhoneNumInput() {
    //this function prompts the client for their phoneNum and return the phoneNum as a string
    
    string phoneNum;
    
    //Prompting the client for their phoneNum
    cout << "Enter Phone Number: " << endl;
    getline(cin, phoneNum); //get the client's phoneNum from the user using the getline() function
    return phoneNum;
}

string getClientAddressInput() {
    //this function prompts the client for their address and return the address as a string
    
    string address;
     //Prompting the client for their address 
    cout << "Enter Client's Address: " << endl;
    getline(cin, address); //get the client's address from the user using the getline() function
    return address;
}

bool getDogShotsInfo() {
    //this function prompts the user to indicate whether their dog is up to date on shots
    //and returns a boolean value indicating whether the dog is up to date on shots
    
    bool isDogUpToDateOnShots;
    
    //prompt the user to indicate whether their dog is up to date on shots.
    cout << "Enter 0 if dog is not up to date on shots, 1 if dog is up to date:" << endl;
    cin >> isDogUpToDateOnShots;
    
    //validate the user's input
    while (isDogUpToDateOnShots != 0 & isDogUpToDateOnShots != 1) {
        cout << "Invalid input. Try again:" << endl;
        cin >> isDogUpToDateOnShots;
    }
    //return whether the dog is up to date on shots
    return isDogUpToDateOnShots;
}

string getDogNameInput() {
    //this function prompts the user for the dog's name and returns it
    
    string dogName;
    
    //prompts the user to enter the dog's name
    cout << "Enter dog name:" << endl;
    cin >> dogName;
    return dogName;
}

string getDogBreedInput() {
    //this function prompts the user for the dog's breed and returns it
    string breed;
    
    //prompts the user to enter the dog's breed
    cout << "Enter dog breed:" << endl;
    cin >> breed;
    return breed;
}

int getDogAgeInput() {
    //this function prompts the user for he dog's age and returns it 
    int age;
    
    //prompts user for the dog's age
    cout << "Enter dog age:" << endl;
    cin >> age;
    
    //validating input - has to be greater than 0
    while (age < 0) {
        cout << "Invalid input. Try again:" << endl;
        cin >> age;
    }
    return age;
}

int getDogWeightInput() {
    //this function prompts the user for the dog's weight and returns it
    
    int weight;
    
    //prompting user to enter in the weight
    cout << "Enter weight (in pounds):" << endl;
    cin >> weight;
    
    //validating input - weight must be more than 0
    while (weight <= 0) {
        cout << "Invalid input. Try again:" << endl;
        cin >> weight;
    }
    return weight;
}

bool getDogHyperInput() {
    //function that prompts the user to indicate whether the dog is hyper or not
    
    bool isHyper;
    
    //Prompts the user to put 0 if dog isn't hyper and 1 if dog is hyper 
    cout << "Enter 0 if dog is not hyper, 1 if dog is:" << endl;
    cin >> isHyper;
    
    //validates user input 
    while (isHyper != 0 & isHyper != 1) {
        cout << "Invalid input. Try again:" << endl;
        cin >> isHyper;
    }
    return isHyper;
}

bool getDogBitesInput() {
    //function that prompts the user to indicate whether the dog bites or not
    
    bool doesBite;
    
    //Prompts the user to put 0 if dog doesn't bite and 1 if dog does bite 
    cout << "Enter 0 if dog does not bite, 1 if dog does:" << endl;
    cin >> doesBite;
    
    //validates user input 
    while (doesBite != 0 & doesBite != 1) {
        cout << "Invalid input. Try again:" << endl;
        cin >> doesBite;
    }
    return doesBite;
}

bool getDogAnxietyInput() {
    //function that prompts the user to indicate whether the dog has anxiet or not
    
    bool hasAnxiety;
    
    //Prompts the user to put 0 if dog doesn't have anxiety and 1 if dog does
    cout << "Enter 0 if dog does not have anxiety, 1 if dog does:" << endl;
    cin >> hasAnxiety;
    
    //validates user input
    while (hasAnxiety != 0 & hasAnxiety != 1) {
        cout << "Invalid input. Try again:" << endl;
        cin >> hasAnxiety;
    }
    return hasAnxiety;
}

client gettingClientInput() {
    //Promts the user for their name, phoneNum, address, and shots, and returns a client object
    
    string clientName = getClientNameInput(); 
    string phoneNum = getClientPhoneNumInput();
    string address = getClientAddressInput();
    bool shots = getDogShotsInfo();
    
    //create a new client object with the user's input
    client clientInput(clientName, phoneNum, address, shots);
    return clientInput;
}

dog gettingDogInput() {
    //prompts the user for their dog's name, breed, age, weight, and behavior, and returns a dog object
    
    string name = getDogNameInput();
    string breed = getDogBreedInput();
    int age = getDogAgeInput();
    int weight = getDogWeightInput();
    bool hyper = getDogHyperInput();
    bool bites = getDogBitesInput();
    bool anxiety = getDogAnxietyInput();
    
    //create a new dog object with the user's input
    dog dogInput(name, breed, age, weight, hyper, bites, anxiety);
    return dogInput;
}

string getFirstNameInput() {
    //prompts the user (sitter) for their first name and returns it
    
    string firstName;
    
    cout << "Enter first name:" << endl;
    cin >> firstName;
    return firstName;
}

string getLastNameInput() {
    //prompts the user (sitter) for their last name and returns it
    
    string lastName;
    
    cout << "Enter last name:" << endl;
    cin >> lastName;
    return lastName;
}

vector<string> getBreedsInput() {
    //prompts the user for their preferred dog breeds and returns a vector of strings containing the breeds
    
    string breed;
    vector<string> breeds;
    
    cout << "Enter breeds you prefer to work with (one at a time) or Q to complete:" << endl;
    cout << "You may enter ALL if you do not have a preference." << endl;
    cin >> breed;
    
    //if the user enters ALL, return a vector containing the string "ALL"
    if (breed == "ALL") {
        breeds.push_back(breed);
        return breeds;
    }
    //while the user does not enter Q, continue reading in breeds and adding them to the vector
    while (breed != "Q") {
        breeds.push_back(breed);
        cin >> breed;
    }
    //return the vector of breeds
    return breeds;
}

int getMinAgeInput() {
    //prompts the user for their minimum age preference and returns it
    
    int minAge;
    
    //prompt the user for their minimum age preference
    cout << "Enter minimum age preference:" << endl;
    cin >> minAge;
    
    //validate the user's input that min age is more than 0
    while (minAge < 0) {
        cout << "Invalid input. Try again:" << endl;
        cin >> minAge;
    }
    return minAge;
}

int getMaxAgeInput(int minAge) {
    //prompts the user for their max age preference and returns it, ensuring that it is greater than or equal to the min age preference
    int maxAge;
    
    //prompt the user for their max age preference
    cout << "Enter maximum age preference:" << endl;
    cin >> maxAge;
    
    //validates user input
    while (maxAge < minAge) {
        cout << "Invalid input. Try again:" << endl;
        cin >> maxAge;
    }
    return maxAge;
}

int getMinWeightInput() {
    //prompts the user for their min weight preference and returns it
    
    int minWeight;
    
    //prompt the user for their minimum weight preference
    cout << "Enter minimum weight preference:" << endl;
    cin >> minWeight;
    
    //validates the user's input is greater than 00
    while (minWeight <= 0) {
        cout << "Invalid input. Try again:" << endl;
        cin >> minWeight;
    }
    return minWeight;
}

int getMaxWeightInput(int minWeight) {
    //prompts the user for their max weight preference and returns it, ensuring that it is greater than or equal to the min weight preference
    
    int maxWeight;
    
    //prompt the user for their maximum weight preference
    cout << "Enter maximum weight preference:" << endl;
    cin >> maxWeight;
    
    //validate the user's input
    while (maxWeight < minWeight) {
        cout << "Invalid input. Try again:" << endl;
        cin >> maxWeight;
    }
    return maxWeight;
}

bool getHyperHandlingInput() {
    //prompts the user to indicate whether they handle hyper dogs and returns it
    
    bool handlesHyper;
    
    // Prompt the user to indicate whether they handle hyper dogs
    cout << "Enter 0 if you do not handle hyper dogs, 1 if you do:" << endl;
    cin >> handlesHyper;
    
    //validate the user's input
    while (handlesHyper != 0 & handlesHyper != 1) {
        cout << "Invalid input. Try again:" << endl;
        cin >> handlesHyper;
    }
    return handlesHyper;
}

bool getBiteHandlingInput() {
    //prompts the user to indicate whether they handle dogs that bite and returns it
    
    bool handlesBites;
    
    //prompt the user to indicate whether they handle dogs that bite
    cout << "Enter 0 if you do not handle biting, 1 if you do:" << endl;
    cin >> handlesBites;
    
    //validate the user's input
    while (handlesBites != 0 & handlesBites != 1) {
        cout << "Invalid input. Try again:" << endl;
        cin >> handlesBites;
    }
    return handlesBites;
}

bool getAnxietyHandlingInput() {
    //prompts the user to indicate whether they handle dogs with anxiety and returns it
    bool handlesAnxiety;
    
    //prompt the user to indicate whether they handle dogs with anxiety
    cout << "Enter 0 if you do not handle anxiety, 1 if you do:" << endl;
    cin >> handlesAnxiety;
    
    //validate the user's input
    while (handlesAnxiety != 0 & handlesAnxiety != 1) {
        cout << "Invalid input. Try again:" << endl;
        cin >> handlesAnxiety;
    }
    return handlesAnxiety;   
}

int validatingMonths(int month) {
    //validates the month input and returns it
    
    //while the month is greater than 12 or less than 1, loop
    while (month > 12 || month < 1) {
        //if the month is 0, break out of the loop
        if (month == 0) {
            break;
        }
        cout << "Error. Invalid input." << endl;
        cout << "Enter a new month:" << endl;
        cin >> month;
    }
    //return the month
    return month;
}

bool validatingDays(int month, vector<int>days) {
    //validates the day input and returns a boolean indicating whether it is valid
    
    bool isValid;
    
    //create a map of months to the number of days in each month
    map<int, int> monthsDays = {
        {1, 31}, {2, 28}, {3, 31}, {4, 30}, {5, 31}, {6, 30},
        {7, 31}, {8, 31}, {9, 30}, {10, 31}, {11, 30}, {12, 31}
    };
    
    // want to validate: not over # of days in month, end not before start
    if (monthsDays.find(month) != monthsDays.end()) {
        if (days[days.size()-1] > monthsDays[month]) {
            isValid = false;
        }
        else if (days[days.size()-2] > monthsDays[month]) {
            isValid = false;
        }
        else if (days[days.size()-1] < days[days.size()-2]) {
            isValid = false;
        }
        else {
            isValid = true;
        }
    }
    //return whether the day input is valid
    return isValid;
}

dogSitter gettingSitterInput() {
    //define a function named 'gettingSitterInput' that returns a 'dogSitter' object
    
    string firstName = getFirstNameInput();
    string lastName = getLastNameInput();
    vector<string> breeds = getBreedsInput();
    int minAge = getMinAgeInput();
    int maxAge = getMaxAgeInput(minAge);
    int minWeight = getMinWeightInput();
    int maxWeight = getMaxWeightInput(minWeight);
    bool handlesHyper = getHyperHandlingInput();
    bool handlesBites = getBiteHandlingInput();
    bool handlesAnxiety = getAnxietyHandlingInput();
    
    //declare an integer variable 'month'
    int month;
    
    //declare a vector of integers 'monthsAvailable'
    vector<int> monthsAvailable;
    
    //declare integer variables 'firstDay' and 'lastDay'
    int firstDay;
    int lastDay;
    
    //declare a vector of integers 'daysAvailable'
    vector<int> daysAvailable;
    
    //prompt the user to enter months available, one at a time, and display instructions
    cout << "Enter months available in number form, one at a time (or 0 to complete):" << endl;
    cin >> month;
    validatingMonths(month);
    
    //enters a loop to collect availability information until the user enters '0' to complete
    while (month != 0) {
        //adds the entered month to the 'monthsAvailable' vector
        monthsAvailable.push_back(month);
        
        //prompt the user to enter the first day of availability
        cout << "Enter first day of availability:" << endl;
        cin >> firstDay;
        
        //add the 'firstDay' to the 'daysAvailable' vector
        daysAvailable.push_back(firstDay);
        
        //prompt the user to enter the last day of availability
        cout << "Enter last day of availability:" << endl;
        cin >> lastDay;
        
        //add the 'lastDay' to the 'daysAvailable' vector
        daysAvailable.push_back(lastDay);
        
        //check if the entered days are valid using 'validatingDays' function
        if (validatingDays(month, daysAvailable) == false) {
            cout << "Invalid input. Try again. ";
            cout << "First day:" << endl;
            cin >> firstDay;
            cout << "Last day:" << endl;
            cin >> lastDay;
        }
        
        //prompt the user to enter the next month
        cout << "Enter month:" << endl;
        cin >> month;
        
        //validate the entered month using 'validatingMonths' function
        validatingMonths(month);
    }
    
    //create a 'dogSitter' object 'sitterInput' with the collected input data
    dogSitter sitterInput(firstName, lastName, breeds, minAge, maxAge, minWeight, maxWeight, handlesHyper, handlesBites, handlesAnxiety, monthsAvailable, daysAvailable);
    
    //return the 'sitterInput' object
    return sitterInput;
}

job gettingJobInput(dogSitter& sitterInput, client& clientInput) {
    //define a function 'gettingJobInput' that takes 'sitterInput' and 'clientInput' as input and returns a 'job' object
    
    //get the drop-off time input from the user and store it in 'dropOffTime'
    string dropOffTime = getDropOffTimeInput();
    
    //get the estimated pick-up time input from the user and store it in 'estimatedPickUpTime'
    string estimatedPickUpTime = getEstimatedPickUpTimeInput();
    
    //calculate the quote for the job based on the drop-off and pick-up times and store it in 'quote'
    double quote = getQuote(dropOffTime, estimatedPickUpTime);
    
    //create a 'job' object 'jobInput' with the collected input data
    job jobInput(sitterInput, clientInput, dropOffTime, estimatedPickUpTime, quote);
    
    //return the 'jobInput' object
    return jobInput;
}

string confirmationRedo(dogSitter sitterInput) {
    //Define a function 'confirmationRedo' that takes 'sitterInput' as input and returns a confirmation string
    
    string confirmation;
    int redo;
    
    //display a menu of areas that can be fixed or updated
    cout << "Which area needs fixed?" << endl;
    cout << "1: First Name" << endl << "2: Last Name" << endl << "3: Breeds" << endl << "4: Min Age" << endl << "5: Max Age" << endl <<
        "6: Min Weight" << endl << "7: Max Weight"  << endl << "8: Hyper Handling" << endl << "9: Bite Handling" << endl << "10: Anxiety Handling" << endl <<
        "11: Availability" << endl;
    
    //read the user's choice and store it in 'redo'
    cin >> redo;
    
    //check the user's choice and update the corresponding information in 'sitterInput'
    if (redo == 1) {
        string name = getFirstNameInput();
        sitterInput.setFirstName(name);
    }
    if (redo == 2) {
        string name = getLastNameInput();
        sitterInput.setLastName(name);
    }
    if (redo == 3) {
        vector<string> breeds = getBreedsInput();
        sitterInput.setBreeds(breeds);
    }
    if (redo == 4) {
        int minAge = getMinAgeInput();
        sitterInput.setMinAge(minAge);
    }
    if (redo == 5) {
        int maxAge = getMaxAgeInput(sitterInput.getMaxWeight());
        sitterInput.setMaxAge(maxAge);
    }
    if (redo == 6) {
        int minWeight = getMinWeightInput();
        sitterInput.setMinWeight(minWeight);
    }
    if (redo == 7) {
        int maxWeight = getMaxWeightInput(sitterInput.getMinWeight());
        sitterInput.setMaxWeight(maxWeight);
    }
    if (redo == 8) {
        bool hyperHandling = getHyperHandlingInput();
        sitterInput.setHandlesHyper(hyperHandling);
    }
    if (redo == 9) {
        bool biteHandling = getBiteHandlingInput();
        sitterInput.setHandlesBites(biteHandling);
    }
    if (redo == 10) {
        bool anxietyHandling = getAnxietyHandlingInput();
        sitterInput.setHandlesAnxiety(anxietyHandling);
    }
    if (redo == 11) {
        int month;
        vector<int> monthsAvailable;
        
        int firstDay;
        int lastDay;
        vector<int> daysAvailable;
        
        cout << "Enter months available in number form, one at a time (or 0 to complete):" << endl;
        cin >> month;
        validatingMonths(month);
        
        while (month != 0) {
            monthsAvailable.push_back(month);
            
            cout << "Enter first day of availability:" << endl;
            cin >> firstDay;
            daysAvailable.push_back(firstDay);
            
            cout << "Enter last day of availability:" << endl;
            cin >> lastDay;
            daysAvailable.push_back(lastDay);
            
            if (validatingDays(month, daysAvailable) == false) {
                cout << "Invalid input. Try again. ";
                cout << "First day:" << endl;
                cin >> firstDay;
                cout << "Last day:" << endl;
                cin >> lastDay;
            }
            
            cout << "Enter month:" << endl;
            cin >> month;
            validatingMonths(month);
        }
        sitterInput.setMonths(monthsAvailable);
        sitterInput.setDays(daysAvailable);
    }
    
    //display the updated 'sitterInput' profile
    cout << "PROFILE:" << endl;
    sitterInput.displaySitterInfo();
    
    //prompt the user to confirm or redo the profile update
    cout << "Enter C if information is now correct or R to redo profile:" << endl;
    cin >> confirmation;
    
    return confirmation;
}

string confirmRedoClient(client clientInput) {
    //define a function 'confirmRedoClient' that takes 'clientInput' as input and returns a confirmation string
    string confirmation;
    int redo;
    
    //Display a menu of areas that can be fixed or updated for the client
    cout << "Which area needs fixed?" << endl;
    cout << "1: Client's Name " << endl << "2: Phone Number " << endl << "3: Address " << endl << "4: Dog Updated On Shots" << endl;
    
    //read the user's choice and store it in 'redo'
    cin >> redo;
    cin.ignore();
    
    //check the user's choice and update the corresponding information in 'clientInput'
    if (redo == 1) {
        string clientName = getClientNameInput();
        clientInput.setClientName(clientName);
    }
    if (redo == 2) {
        string phoneNum = getClientPhoneNumInput();
        clientInput.setPhoneNum(phoneNum);
    }
    if (redo == 3) {
        string address = getClientAddressInput();
        clientInput.setAddress(address);
    }
    if (redo == 4) {
        bool shots = getDogShotsInfo();
        clientInput.setDogHasShots(shots);
    }
    
    //display the updated 'clientInput' profile
    cout << "CLIENT PROFILE:" << endl;
    clientInput.displayClientInfo();
    
    //prompt the user to confirm or redo the profile update
    cout << "Enter C if information is now correct or R to redo profile:" << endl;
    cin >> confirmation;
    
    return confirmation;
}

bool compareSitters(dogSitter& sitter1, dogSitter& sitter2, dog& dogInput) {
    //define a function 'compareSitters' that compares two sitters based on their matching percentages with a dog
    
    //calculate the matching percentages for 'sitter1' and 'sitter2' with 'dogInput'
    int matchPercentageA = matchingProcess(dogInput, sitter1);
    int matchPercentageB = matchingProcess(dogInput, sitter2);
    
    //compare the matching percentages and return 'true' if 'sitter1' has a higher percentage
    return matchPercentageA > matchPercentageB;
}

int main() {
    string role;
    string confirmation;
    int redo;
    
    //create a vector 'allSitters' to store information about dog sitters
    vector<dogSitter> allSitters;
    
    //create sample dog sitters and add them to the 'allSitters' vector.
    //(similarly, you can add more sitters)
    dogSitter sampleSitter1("Izabelle", "Lantz", {"ALL"}, 2, 12, 2, 40, true, true, false, {1, 3, 10}, {2, 12, 3, 24, 15, 25});
    dogSitter sampleSitter2("Tiffany", "Monroe", {"Huskey", "Chihuhua"}, 1, 10, 1, 60, false, false, true, {5, 11}, {1, 20, 16, 23});
    dogSitter sampleSitter3("Preston", "Mesecher", {"Bulldog"}, 3, 15, 5, 40, true, false, true, {2, 6, 12}, {12, 22, 14, 16, 22, 29});
    
    // Add 'sampleSitter1' to the 'allSitters' vector
    allSitters.push_back(sampleSitter1);
    allSitters.push_back(sampleSitter2);
    allSitters.push_back(sampleSitter3);
    
    //prompt the user to choose a role (sitter or client) and store it in 'role'
    cout << "Are you a sitter or a client?" << endl;
    cout << "Enter S for sitter, C for client:" << endl;
    cin >> role;
    
    //validate the user's input
    while (role != "S" && role != "C") {
        cout << "Invalid input. Try again:" << endl;
        cin >> role;
    }
    
    //handles the sitter's role
    if (role == "S") {
        // Get sitter information and display it
        dogSitter sitterInput = gettingSitterInput();
        cout << "SITTER PROFILE:" << endl;
        sitterInput.displaySitterInfo();
        
        //prompt the user to confirm or redo the sitter's profile
        cout << "Enter C if information is correct or R to change information:" << endl;
        cin >> confirmation;
        
        //handle user's choice regarding the confirmation
        while (confirmation != "C" && confirmation != "R") {
            cout << "Invalid input. Try again:" << endl;
            cin >> confirmation;
        }
        
        if (confirmation == "C") {
            cout << "Thank you for registering to take care of our clients animals! You will hear from someone soon if they are interested." << endl;
        }
        else if (confirmation == "R") {
            //handle user's choice regarding the confirmation
            confirmation = confirmationRedo(sitterInput);
            if (confirmation == "R") {
                //handle user's choice regarding the confirmation
                sitterInput = gettingSitterInput();
            }
            cout << "Thank you for registering to take care of our clients animals! You will hear from someone soon if they are interested." << endl;
            
        }
        else {
            cout << "Invalid input. Try again." << endl;
            cin >> confirmation;
        }
        abort();
    }
    
    if (role == "C") {
        // use this line ---------- to get client information to fill client class before getting dog information
        //handles the clients role
        //get client information and display it
        client clientInput = gettingClientInput();
        cout << "CLIENT PROFILE:" << endl;
        clientInput.displayClientInfo();
        
        //prompt the user to confirm or redo the sitter's profile
        cout << "Enter C if information is correct or R to change information:" << endl;
        cin >> confirmation;
        
        //handle user's choice regarding the confirmation
        while (confirmation != "C" && confirmation != "R") {
            cout << "Invalid input. Try again:" << endl;
            cin >> confirmation;
        }
        
        if (confirmation == "C") {
            cout << "Thank you for your input. Next up is the dog's information." << endl;
        }
        else if (confirmation == "R") {
            //handle user's choice regarding the confirmation
            confirmRedoClient(clientInput);
            if (confirmation == "R") {
                //handle user's choice regarding the confirmation
                clientInput = gettingClientInput();
            }
            cout << "Thank you for your input. Next up is the dog's information." << endl;
            
        }
        else {
            cout << "Invalid input. Try again." << endl;
            cin >> confirmation;
        }
        
        //get dog's information and display it
        dog dogInput = gettingDogInput();
        cout << "DOG PROFILE:" << endl;
        //use this line ---------- to display dog profile
        dogInput.displayDogInfo();
        
        //handle user's choice regarding the confirmation
        cout << "Enter C if information is correct or R to change information:" << endl;
        cin >> confirmation;
        
        if (confirmation == "C") {
            cout << "Thank you for registering to find a dog sitter! Your list of potential sitters will appear in one second." << endl;
            cout << endl;
            sort(allSitters.begin(), allSitters.end(), [&dogInput](dogSitter& sitter1, dogSitter& sitter2) {
                return compareSitters(sitter1, sitter2, dogInput);
            });
            
            for (dogSitter& sitter : allSitters) {
                sitter.displaySitterInfo();
                cout << "Match %: " << matchingProcess(dogInput, sitter) << endl;
                cout << endl;
            }
        }
        else if (confirmation == "R") {
            dogInput = gettingDogInput();
            cout << "Thank you for registering to find a dog sitter! Your list of potential sitters will appear in one second." << endl;
            cout << endl;
            sort(allSitters.begin(), allSitters.end(), [&dogInput](dogSitter& sitter1, dogSitter& sitter2) {
                return compareSitters(sitter1, sitter2, dogInput);
            });
            
            for (dogSitter& sitter : allSitters) {
                sitter.displaySitterInfo();
                cout << "Match %: " << matchingProcess(dogInput, sitter) << endl;
                cout << endl;
            }
        }
        else {
            cout << "Invalid input. Try again." << endl;
            cin >> confirmation;
        }
        
        //get job information and display it
        job jobInput = gettingJobInput(allSitters[0],  clientInput);
        cout << "JOB PROFILE: " << endl;
            //use this line ---------- to display dog profile
        jobInput.displayJobInfo();
        abort();
    }
    return 0;
};
